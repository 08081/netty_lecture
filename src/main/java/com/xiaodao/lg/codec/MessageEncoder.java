package com.xiaodao.lg.codec;

import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageEncoder;
import io.netty.util.CharsetUtil;

import java.security.spec.MGF1ParameterSpec;
import java.util.List;

/**
 * @author qp zhang
 * @description 消息的编码
 * 泛型是因为 写消息的时候string
 */
public class MessageEncoder extends MessageToMessageEncoder<String> {


    @Override
    protected void encode(ChannelHandlerContext ctx, String msg, List<Object> out) throws Exception {
        System.out.println("消息正在进行编码");
        //传递到下一个出站的handler
        out.add(Unpooled.copiedBuffer(msg, CharsetUtil.UTF_8));
    }
}
