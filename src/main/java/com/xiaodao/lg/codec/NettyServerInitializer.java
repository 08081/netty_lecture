package com.xiaodao.lg.codec;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;

/**
 * Created by xiaodao
 * date: 2019/11/29
 */
public class NettyServerInitializer extends ChannelInitializer<SocketChannel> {


    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ChannelPipeline pipeline = ch.pipeline();
        //添加解码器
     //   pipeline.addLast("messageDecoder", new MessageDecoder());
        //添加编码器
       // pipeline.addLast("messageEncoder",new MessageEncoder());
        pipeline.addLast(new MessageCodec());
        pipeline.addLast("testHttpServerHandler",new NettyHttpServerHandler());

    }
}
