package com.xiaodao.lg.test01;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;

/**
 * @author qp zhang
 * @description
 */
public class NettyClient {

    public static void main(String[] args) throws InterruptedException {
        EventLoopGroup group = new NioEventLoopGroup();
        //2.创建启动助手
        Bootstrap bootstrap = new Bootstrap();
        //3 设置线程组
        bootstrap.group(group)
                .channel(NioSocketChannel.class)
                .handler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel ch) throws Exception {
                        ch.pipeline().addLast(new NettyClientHandler());
                    }
                });
        //7 启动客户端,等待连接服务端,同时异步改为同步
        ChannelFuture channelFuture = bootstrap.connect("127.0.0.1", 8899).sync();
        //8. 关闭通道和关闭连接
        channelFuture.channel().closeFuture().sync();
        group.shutdownGracefully();
    }
}
