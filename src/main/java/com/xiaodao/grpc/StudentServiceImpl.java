package com.xiaodao.grpc;

import com.xiaodao.proto.*;
import io.grpc.stub.StreamObserver;

import java.util.UUID;

/**
 * Created by xiaodao
 * date: 2019/12/3
 */
public class StudentServiceImpl extends StudentServiceGrpc.StudentServiceImplBase {

    @Override
    public void getRealNameByUsername(MyRequest request, StreamObserver<MyResponse> responseObserver) {

        System.out.println("接受客户端信息:"+ request.getUsername());
        responseObserver.onNext(MyResponse.newBuilder().setRealname("张小刀").build());
        responseObserver.onCompleted();
    }

    /**
     * 服务端流式的数据
     * @param request
     * @param responseObserver
     */
    @Override
    public void getStudentsByAge(StudentRequest request, StreamObserver<StudentResponse> responseObserver) {
        System.out.println("[服务端流方式]接受客户端信息: "+ request.getAge());
        responseObserver.onNext(StudentResponse.newBuilder().setAge(20).setCity("北京").setName("张飒1").build());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        responseObserver.onNext(StudentResponse.newBuilder().setAge(21).setCity("北京").setName("张飒2").build());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        responseObserver.onNext(StudentResponse.newBuilder().setAge(22).setCity("北京").setName("张飒3").build());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        responseObserver.onNext(StudentResponse.newBuilder().setAge(23).setCity("北京").setName("张飒4").build());
        responseObserver.onCompleted();
    }

    /**
     * 客户端 流式请求 ->服务端返回
     * @param responseObserver
     * @return
     */
    @Override
    public StreamObserver<StudentRequest> getStudentsWrapperByAge(StreamObserver<StudentResponseList> responseObserver) {
        System.out.println("客户端 流式调用  ");
    return  new StreamObserver<StudentRequest>() {
        int StudentRequest ;
        StudentRequest previous;

        @Override
        public void onNext(StudentRequest value) {
            System.out.println("value:" + value.getAge());
            StudentRequest ++;
//            if(RouteGuideUtil)
        }

        @Override
        public void onError(Throwable t) {
            System.out.println("error:"+ t.getMessage());

        }

        /**
         * 客户端一个一个 发送过来,服务端然后在返回
         */
        @Override
        public void onCompleted() {
            StudentResponse  studentResponse =  StudentResponse.newBuilder().setAge(12).setName("张三").setCity("北京").build();
            StudentResponse  studentResponse2 =  StudentResponse.newBuilder().setAge(20).setName("张si").setCity("山西").build();

            StudentResponseList studentResponseList = StudentResponseList.newBuilder().addStudentResponse(studentResponse).addStudentResponse(studentResponse2).build();

            responseObserver.onNext(studentResponseList);
            responseObserver.onCompleted();
        }
    };
    }


    /**
     * 双向
     * @param responseObserver
     * @return
     */
    @Override
    public StreamObserver<StreamRequest> biTalk(StreamObserver<StreamResponse> responseObserver) {
        return new StreamObserver<StreamRequest>() {
            @Override
            public void onNext(StreamRequest value) {
                System.out.println("请求数据:"+ value.getRequestInfo());
                responseObserver.onNext(StreamResponse.newBuilder().setResponseInfo(UUID.randomUUID().toString()).build());
            }

            @Override
            public void onError(Throwable t) {
                System.out.println(t.getMessage());
            }

            @Override
              public void onCompleted() {
                responseObserver.onCompleted();
            }
        };
    }
}
