package com.xiaodao.grpc;

import com.sun.prism.shader.Solid_TextureYV12_AlphaTest_Loader;
import com.xiaodao.proto.*;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;

import javax.swing.*;
import java.time.LocalDateTime;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

/**
 * Created by xiaodao
 * date: 2019/12/3
 */
public class GrpcClient {

    private final ManagedChannel channel;

    private final StudentServiceGrpc.StudentServiceBlockingStub blockingStub;

    public GrpcClient (String host,int port){
        this(ManagedChannelBuilder.forAddress(host,port).usePlaintext(true));

    }
    GrpcClient(ManagedChannelBuilder channelBuilder){
        channel  = channelBuilder.build();
        blockingStub =  StudentServiceGrpc.newBlockingStub(channel);
    }
    public void shutdown() throws InterruptedException {
        channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
    }


    public static void main(String[] args) throws InterruptedException {
       /* GrpcClient client = new GrpcClient("localhost",8899);
        MyRequest request = MyRequest.newBuilder().setUsername("xxxx").build();
        MyResponse response = client.blockingStub.getRealNameByUsername(request);
        System.out.println(response.getRealname());*/

        ManagedChannel managedChannel = ManagedChannelBuilder.forAddress("localhost", 8899)
                .usePlaintext(true).build();
        StudentServiceGrpc.StudentServiceStub  studentServiceStub =  StudentServiceGrpc.newStub(managedChannel);
        StudentServiceGrpc.StudentServiceBlockingStub blockingStub = StudentServiceGrpc.newBlockingStub(managedChannel);
       //simple rpc
        MyResponse response = blockingStub.getRealNameByUsername(MyRequest.newBuilder().setUsername("张三").build());
        System.out.println(response.getRealname());


     /* //第二种方式
        Iterator<StudentResponse> studentsByAge = blockingStub.getStudentsByAge(StudentRequest.newBuilder().setAge(20).build());

        while (studentsByAge.hasNext()){
            StudentResponse next = studentsByAge.next();
            System.out.println("服务端返回数据: "+ next.getName()+" - "+next.getAge()+" - "+next.getCity());
        }*/

        /**
         * client stream
         */

      /*  StreamObserver<StudentResponseList> studentResponseListStreamObserver =  new StreamObserver<StudentResponseList>() {
            *//**
             * 服务端返回的结果
             * @param value
             *//*
            @Override
            public void onNext(StudentResponseList value) {
            value.getStudentResponseList().forEach(it->{
                System.out.println(it.getName() +"- "+ it.getAge()+"- "+ it.getCity());
                System.out.println("***----***");
            });
            }

            @Override
            public void onError(Throwable t) {

            }

            @Override
            public void onCompleted() {
                System.out.println("GrpcClient.onCompleted");
            }
        };*/
     /*   //客户端以流式的方式 请求服务端.那就是异步的
        StreamObserver<StudentRequest> studentRequestStreamObserver =  studentServiceStub.getStudentsWrapperByAge(studentResponseListStreamObserver);

        studentRequestStreamObserver.onNext(StudentRequest.newBuilder().setAge(21).build());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        studentRequestStreamObserver.onNext(StudentRequest.newBuilder().setAge(21).build());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        studentRequestStreamObserver.onNext(StudentRequest.newBuilder().setAge(21).build());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        studentRequestStreamObserver.onNext(StudentRequest.newBuilder().setAge(21).build());
        studentRequestStreamObserver.onCompleted();*/
/*
        StreamObserver<StreamRequest> requestStreamObserver =  studentServiceStub.biTalk(new StreamObserver<StreamResponse>() {
            @Override
            public void onNext(StreamResponse value) {
                System.out.println(value.getResponseInfo());
            }

            @Override
            public void onError(Throwable t) {
                System.out.println(t.getMessage());
            }

            @Override
            public void onCompleted() {
                System.out.println( "onCompleted");

            }
        });

        for (int i = 0; i <10 ; i++) {
            requestStreamObserver.onNext(StreamRequest.newBuilder().setRequestInfo(LocalDateTime.now().toString()).build());
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }*/


    }
}
