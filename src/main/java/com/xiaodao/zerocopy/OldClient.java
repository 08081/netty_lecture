package com.xiaodao.zerocopy;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.Socket;

/**
 * Created by xiaodao
 * date: 2019/12/8
 */
public class OldClient {

    public static void main(String[] args) throws Exception {

        Socket socket =  new Socket("localhost",8899);
        String fileName = "/opt/software/hbase-1.2.0-bin.tar.gz";
        FileInputStream fileInputStream = new FileInputStream(new File(fileName));

        DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
        byte[]  bytes = new byte[4096];
        long readCount;
        long total =0;
        long startTime = System.currentTimeMillis();

        while ((readCount = fileInputStream.read(bytes))>=0){
            total +=readCount;
            dataOutputStream.write(bytes);
        }
        System.out.println( "发送总字节数: "+ total +"   耗时   "+ (System.currentTimeMillis()-startTime));
        dataOutputStream.close();
        fileInputStream.close();;
        socket.close();
    }
}
