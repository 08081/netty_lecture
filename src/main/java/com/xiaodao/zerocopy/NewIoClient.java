package com.xiaodao.zerocopy;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.FileChannel;
import java.nio.channels.SocketChannel;

/**
 * Created by xiaodao
 * date: 2019/12/8
 */
public class NewIoClient {

    public static void main(String[] args) throws IOException {
        SocketChannel socketChannel = SocketChannel.open();
        socketChannel.connect(new InetSocketAddress(8899));
        socketChannel.configureBlocking(true);
        String fileName = "/opt/software/hbase-1.2.0-bin.tar.gz";

        FileChannel fileChannel = new FileInputStream(fileName).getChannel();

        long start =System.currentTimeMillis();
        long transfer = fileChannel.transferTo(0, fileChannel.size(), socketChannel);
        System.out.println("发送总字节数: "+ transfer +"耗时: "+(System.currentTimeMillis()-start));
        fileChannel.close();
    }
}
