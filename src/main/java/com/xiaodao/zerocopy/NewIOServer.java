package com.xiaodao.zerocopy;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.nio.ByteBuffer;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

/**
 * Created by xiaodao
 * date: 2019/12/8
 */
public class NewIOServer {

    public static void main(String[] args) throws Exception {
        ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
        ServerSocket socket = serverSocketChannel.socket();
        socket.setReuseAddress(true);//超时的端口,别的端口也可以绑定
         serverSocketChannel.bind(new InetSocketAddress(8899));

        ByteBuffer allocate = ByteBuffer.allocate(4096);
        while (true){
            //这个一定是一个阻塞方法
            SocketChannel socketChannel = serverSocketChannel.accept();
            int read =0 ;
            while (read !=-1){
                read = socketChannel.read(allocate);
                allocate.rewind();
            }
        }


    }
}
