package com.xiaodao.nio;

import java.nio.ByteBuffer;

/**
 * 只读buffer
 * Created by xiaodao
 * date: 2019/12/7
 */
public class NioTest7 {

    public static void main(String[] args) {
        ByteBuffer byteBuffer = ByteBuffer.allocate(20);
        for (int i = 0; i <10 ; i++) {
            byteBuffer.put((byte)i);

        }

        ByteBuffer readOnlyBuffer = byteBuffer.asReadOnlyBuffer();

        System.out.println(readOnlyBuffer.getClass());
        readOnlyBuffer.put(1,(byte) 2);

    }
}
