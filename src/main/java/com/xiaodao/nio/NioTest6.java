package com.xiaodao.nio;

import java.nio.ByteBuffer;

/**
 * Created by xiaodao
 * date: 2019/12/7
 */
public class NioTest6 {

    public static void main(String[] args) {
        ByteBuffer byteBuffer  = ByteBuffer.allocate(10);

        for (int i = 0; i <10 ; i++) {
            byteBuffer.put((byte)i);

        }

        byteBuffer.position(2);
        byteBuffer.limit(6);
        ByteBuffer slice = byteBuffer.slice();

        for (int i = 0; i <slice.capacity() ; i++) {
            byte b = slice.get();
            b *= 2;
            slice.put(i,b);

        }

        byteBuffer.position(0).limit(byteBuffer.capacity());
        System.out.println("position:"+ byteBuffer.position());
        System.out.println("limit:"+ byteBuffer.limit());

        while (byteBuffer.hasRemaining()){
            System.out.print(byteBuffer.get()+"-");
        }

    }
}
