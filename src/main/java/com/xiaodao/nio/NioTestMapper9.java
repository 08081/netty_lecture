package com.xiaodao.nio;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

/**
 * Created by xiaodao
 * date: 2019/12/7
 */
public class NioTestMapper9 {

    public static void main(String[] args) throws IOException {
        RandomAccessFile randomAccessFile =  new RandomAccessFile("niotest9.txt","rw");

        FileChannel channel = randomAccessFile.getChannel();
        MappedByteBuffer map = channel.map(FileChannel.MapMode.READ_WRITE, 0, 5);
        map.put(0,(byte)'t');
        map.put(4,(byte)'p');
        randomAccessFile.close();




    }
}
