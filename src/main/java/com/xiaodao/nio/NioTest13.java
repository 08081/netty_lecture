package com.xiaodao.nio;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;

/**
 * Created by xiaodao
 * date: 2019/12/8
 * 编码解码
 */
public class NioTest13 {

    public static void main(String[] args) throws IOException {

        String inputFile = "NioTest13-input.txt";
        String outFile = "NioTest13-outFile.txt";


        RandomAccessFile inputRandomAccessFile = new RandomAccessFile(inputFile, "r");
        RandomAccessFile outputRandomAccessFile = new RandomAccessFile(outFile, "rw");

        long length = new File(inputFile).length();
        FileChannel channel = inputRandomAccessFile.getChannel();


        FileChannel channel1 = outputRandomAccessFile.getChannel();
        MappedByteBuffer inputData = channel.map(FileChannel.MapMode.READ_ONLY, 0, length);
        Charset.availableCharsets().forEach((a,b)->{
            System.out.println("a:" +a+ " b:"+ b);
        });
        /**
         * 你好 你3个字节
         * AA BB CC EE FF CC
         * iso-8891-1 一个字节
         */
        Charset charset = Charset.forName("iso-8859-1");
        CharsetEncoder encoder = charset.newEncoder();
        CharsetDecoder decoder = charset.newDecoder();

        CharBuffer charBuffer = decoder.decode(inputData);
        ByteBuffer byteBuffer = encoder.encode(charBuffer);

        channel1.write(byteBuffer);
        channel.close();
        channel1.close();
    }
}
