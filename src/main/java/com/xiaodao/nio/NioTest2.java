package com.xiaodao.nio;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.channels.FileChannel;
import java.security.SecureRandom;

/**
 * Created by xiaodao
 * date: 2019/12/5
 */
public class NioTest2 {

    public static void main(String[] args) throws Exception {
        FileInputStream inputStream = new FileInputStream(new File("niotest2.txt"));
        FileChannel channel = inputStream.getChannel();
        ByteBuffer byteBuffer = ByteBuffer.allocate(512);
        System.out.println("capacity: "+ byteBuffer.capacity()+" position: " + byteBuffer.position() +"  limit: "+ byteBuffer.limit());
        int read = channel.read(byteBuffer);
        System.out.println("capacity: "+ byteBuffer.capacity()+" position: " + byteBuffer.position() +" limit: "+ byteBuffer.limit());
        byteBuffer.flip();
        System.out.println("capacity: "+ byteBuffer.capacity()+" position: " + byteBuffer.position() +" limit: "+ byteBuffer.limit());
        while (byteBuffer.remaining() > 0){
            byte b = byteBuffer.get();
            System.out.println((char)b);
        }
    }
}
