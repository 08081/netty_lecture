package com.xiaodao.nio;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * Created by xiaodao
 * date: 2019/12/5
 */
public class NioTest3 {

    public static void main(String[] args) throws Exception {
        FileOutputStream outputStream = new FileOutputStream(new File("niotest3.txt"));
        FileChannel channel = outputStream.getChannel();
        ByteBuffer byteBuffer = ByteBuffer.allocate(1024);
        byte[] bytes = "hello word welcome to nio".getBytes();
        System.out.println("capacity: "+ byteBuffer.capacity()+" position: " + byteBuffer.position() +" limit: "+ byteBuffer.limit());
        for (int i = 0; i <bytes.length ; i++) {
            byteBuffer.put(bytes[i]);

        }
        System.out.println("capacity: "+ byteBuffer.capacity()+" position: " + byteBuffer.position() +" limit: "+ byteBuffer.limit());
        byteBuffer.flip();
        System.out.println("2capacity: "+ byteBuffer.capacity()+" position: " + byteBuffer.position() +" limit: "+ byteBuffer.limit());
        byteBuffer.flip();
        System.out.println("3capacity: "+ byteBuffer.capacity()+" position: " + byteBuffer.position() +" limit: "+ byteBuffer.limit());
        channel.write(byteBuffer);
        outputStream.close();
        channel.close();
    }
}
