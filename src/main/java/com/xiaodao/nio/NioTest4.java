package com.xiaodao.nio;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Created by xiaodao
 * date: 2019/12/6
 */
public class NioTest4 {

    public static void main(String[] args) throws IOException {
        FileInputStream inputStream =  new FileInputStream(new File("niotest4.txt"));
        FileOutputStream outputStream = new FileOutputStream(new File("niotest4-write.txt"));
        FileChannel channel = inputStream.getChannel();
        FileChannel outputStreamChannel = outputStream.getChannel();
        ByteBuffer byteBuffer =  ByteBuffer.allocate(512);

        while (true){

            //读完返回-1
            int read = channel.read(byteBuffer);
            System.out.println(read);
            if(read == - 1){
                break;
            }
            byteBuffer.flip();
            outputStreamChannel.write(byteBuffer);
            System.out.println("wirte:"+ read);

        }





    }

}
