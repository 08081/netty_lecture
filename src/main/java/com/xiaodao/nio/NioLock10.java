package com.xiaodao.nio;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;

/**
 * Created by xiaodao
 * date: 2019/12/7
 */
public class NioLock10 {

    public static void main(String[] args) throws IOException {
        RandomAccessFile randomAccessFile =  new RandomAccessFile("niotest10.txt","rw");

        FileChannel channel = randomAccessFile.getChannel();
        FileLock lock = channel.lock(3, 6, true);
        System.out.println("valid: "+ lock.isValid());
        System.out.println("lock type: "+ lock.isShared());

        lock.release();
        randomAccessFile.close();
    }
}
