package com.xiaodao.nio;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * Created by xiaodao
 * date: 2019/12/7
 */
public class NioTest8 {

    public static void main(String[] args) throws IOException {

        FileInputStream  inputStream1=  new FileInputStream(new File("input2.txt"));
        FileOutputStream outputStream = new FileOutputStream(new File("output2.txt"));
        FileChannel channel = inputStream1.getChannel();
        FileChannel outputStreamChannel = outputStream.getChannel();
        ByteBuffer byteBuffer =  ByteBuffer.allocateDirect(512);

        while (true){
            byteBuffer.clear();
            int read = channel.read(byteBuffer);
            System.out.println("read"+  read);
            if(read == - 1){
                break;
            }
            byteBuffer.flip();
            outputStreamChannel.write(byteBuffer);
            System.out.println("wirte:"+ read);

        }





    }

}
