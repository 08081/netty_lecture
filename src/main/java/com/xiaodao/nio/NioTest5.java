package com.xiaodao.nio;

import java.nio.ByteBuffer;

/**
 * 类型化的get 和put
 * Created by xiaodao
 * date: 2019/12/7
 */
public class NioTest5 {
    public static void main(String[] args) {
        ByteBuffer byteBuffer = ByteBuffer.allocate(512);
        byteBuffer.putInt(15);
        byteBuffer.putDouble(13.12121d);
        byteBuffer.putLong(1212111111111L);
        byteBuffer.putShort((short) 1);
        byteBuffer.putChar('你');
        byteBuffer.flip();
        System.out.println(byteBuffer.getInt());
        System.out.println(byteBuffer.getDouble());
        System.out.println(byteBuffer.getLong());
        System.out.println(byteBuffer.getShort());
        System.out.println(byteBuffer.getChar());
    }
}
