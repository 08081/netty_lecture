package com.xiaodao.netty.firestexample;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoop;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;

/**
 * Created by xiaodao
 * date: 2019/11/29
 */
public class TestServer {
    public static void main(String[] args) throws InterruptedException {
        //处理网络连接事件
        EventLoopGroup bossGroup = new NioEventLoopGroup(1);
        //处理真正的连接 读写事件
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        try {


            ServerBootstrap serverBootstrap =  new ServerBootstrap();
            serverBootstrap.group(bossGroup, workerGroup).channel(NioServerSocketChannel.class)

                    .childHandler(new TestServerInitializer())
                    //参数设置
                    .childOption(ChannelOption.SO_BACKLOG, 128)
                    .childOption(ChannelOption.SO_KEEPALIVE, Boolean.TRUE);
            ChannelFuture channelFuture = serverBootstrap.bind(8899).sync();
            //10.关闭通道(并不是真正意义的关闭,而是监听通道关闭的状态)和关闭连接
            channelFuture.channel().closeFuture().sync();
        } finally {
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }

    }
}
