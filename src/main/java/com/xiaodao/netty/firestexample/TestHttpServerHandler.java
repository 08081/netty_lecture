package com.xiaodao.netty.firestexample;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.*;
import io.netty.util.CharsetUtil;

import java.net.URI;


/**
 * Created by xiaodao
 * date: 2019/11/29
 */
public class TestHttpServerHandler extends SimpleChannelInboundHandler<HttpObject> {
    /**
     * 通道读取完毕事件
     * @param ctx
     * @throws Exception
     */
    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        System.out.println("读取完毕");
        //消息出站
        ctx.writeAndFlush(Unpooled.copiedBuffer("你好,我是netty服务端", CharsetUtil.UTF_8));
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
        ctx.close();
    }

    /**
     * 读取客户端发过来的请求.并返回相应
     * @param ctx
     * @param msg
     * @throws Exception
     */
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, HttpObject msg) throws Exception {
        System.out.println(msg.getClass());
        System.out.println(ctx.channel().remoteAddress());
        Thread.sleep(1_000);
        if(msg instanceof HttpRequest){

            HttpRequest httpRequest =(HttpRequest)msg;
            URI uri = new URI(httpRequest.uri());
            System.out.println("请求方法名: "+ httpRequest.method().name()+"  url = "+uri.getPath());

            if(uri.getPath().equals("/favicon.ico")){
                System.out.println("请求:favicon.ico");
                return;
            }
            ByteBuf content = Unpooled.copiedBuffer("Hello word", CharsetUtil.UTF_8);
            DefaultFullHttpResponse response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK, content);
            response.headers().set(HttpHeaderNames.CONTENT_LENGTH,content.readableBytes());
            response.headers().set(HttpHeaderNames.CONTENT_TYPE,"text/plain");
            ctx.writeAndFlush(response);
//            ctx.channel().close();
        }


    }

    /**
     * 3
     * @param ctx
     * @throws Exception
     */
    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        System.out.println("TestHttpServerHandler.channelActive");
        super.channelActive(ctx);
    }

    /**
     * 2
     * @param ctx
     * @throws Exception
     */
    @Override
    public void channelRegistered(ChannelHandlerContext ctx) throws Exception {
        System.out.println("TestHttpServerHandler.channelRegistered");
        super.channelRegistered(ctx);
    }

    /**
     * 1
     * @param ctx
     * @throws Exception
     */
    @Override
    public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
        System.out.println("TestHttpServerHandler.handlerAdded");
        super.handlerAdded(ctx);
    }

    /**
     * 4
     * @param ctx
     * @throws Exception
     */
    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        System.out.println("TestHttpServerHandler.channelInactive");
        super.channelInactive(ctx);
    }

    /**
     * 5
     * @param ctx
     * @throws Exception
     */
    @Override
    public void channelUnregistered(ChannelHandlerContext ctx) throws Exception {
        System.out.println("TestHttpServerHandler.channelUnregistered");
        super.channelUnregistered(ctx);
    }
}
