package com.xiaodao.netty.protobufexample;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * Created by xiaodao
 * date: 2019/12/1
 */
public class MyClientHandler extends SimpleChannelInboundHandler<MyDateInfo.Person> {
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, MyDateInfo.Person msg) throws Exception {

    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {

        MyDateInfo.Person person = MyDateInfo.Person.newBuilder().setName("张小刀").setAge(27).setAddress("北京").build();
        ctx.writeAndFlush(person);
    }
}
