package com.xiaodao.netty.protobufexample.multiple;

import cn.hutool.json.JSONUtil;
import com.xiaodao.netty.protobufexample.MyDateInfo;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * Created by xiaodao
 * date: 2019/12/1
 */
public class MultipleServerHandler extends SimpleChannelInboundHandler<MyDataInfo.MyMessage> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, MyDataInfo.MyMessage msg) throws Exception {
        MyDataInfo.MyMessage.DataType dataType = msg.getDataType();
        System.out.println(JSONUtil.toJsonPrettyStr(msg));
        if(dataType == MyDataInfo.MyMessage.DataType.StudentType){
            MyDataInfo.Student student = msg.getStudent();
            System.out.println(student.getName()+"- "+student.getAge() +" - "+student.getAddress());

        }else if(dataType == MyDataInfo.MyMessage.DataType.DogType){
            MyDataInfo.Dog dog = msg.getDog();
            System.out.println(dog.getName()+" - "+dog.getAge() );
        }else {
            MyDataInfo.Cat cat = msg.getCat();
            System.out.println(cat.getName()+" - "+ cat.getCity());
        }
    }
}
