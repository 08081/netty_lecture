package com.xiaodao.netty.protobufexample;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * Created by xiaodao
 * date: 2019/12/1
 */
public class MyServerHandler extends SimpleChannelInboundHandler<MyDateInfo.Person> {
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, MyDateInfo.Person msg) throws Exception {
        Channel channel = ctx.channel();
        System.out.println(msg.getName());
        System.out.println(msg.getAge());
        System.out.println(msg.getAddress());
    }
}
