package com.xiaodao.thrift;

import org.apache.thrift.TException;
import thrift.generated.DataException;
import thrift.generated.Person;
import thrift.generated.PersonService;

/**
 * Created by xiaodao
 * date: 2019/12/2
 */
public class PersonServiceImpl implements PersonService.Iface {
    @Override
    public Person getPersonByUsername(String username) throws DataException, TException {
        System.out.println("PersonServiceImpl.getPersonByUsername"+ username);
        Person person  = new Person();
        person.setUsername("张小刀");
        person.setAge(39);
        person.setMarried(false);
        return person;
    }

    @Override
    public void savePerson(Person person) throws DataException, TException {
        System.out.println(person.getUsername()+" - "+ person.getAge() +" -- "+person.isMarried());
        System.out.println(person.toString());
    }
}
