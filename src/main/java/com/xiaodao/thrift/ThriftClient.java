package com.xiaodao.thrift;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import org.apache.thrift.transport.TTransportException;
import thrift.generated.Person;
import thrift.generated.PersonService;

/**
 * Created by xiaodao
 * date: 2019/12/2
 */
public class ThriftClient {

    public static void main(String[] args) throws TTransportException {
        TTransport tTransport =  new TFramedTransport(new TSocket("localhost",8899),600);
        TCompactProtocol compactProtocol = new TCompactProtocol(tTransport);
        PersonService.Client client = new PersonService.Client(compactProtocol);

        try {
            tTransport.open();
            Person zhang = client.getPersonByUsername("zhang");
            System.out.println(zhang.toString());
            System.out.println("----");
            Person person = new Person();
            person.setAge(33300);
            person.setUsername("发送名字");
            person.setMarried(true);
            client.savePerson(person);
        } catch (TException e) {
            e.printStackTrace();
        } finally {
            tTransport.close();
        }

    }
}
