package com.xiaodao.thrift;

import org.apache.thrift.TProcessorFactory;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.server.THsHaServer;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TNonblockingServerSocket;
import org.apache.thrift.transport.TTransportException;
import org.apache.thrift.transport.TTransportFactory;
import thrift.generated.Person;
import thrift.generated.PersonService;

/**
 * Created by xiaodao
 * date: 2019/12/2
 */
public class ThriftServer {
    public static void main(String[] args) throws Exception {
        TNonblockingServerSocket serverSocket =   new TNonblockingServerSocket(8899);
        THsHaServer.Args arg = new THsHaServer.Args(serverSocket).minWorkerThreads(2).maxWorkerThreads(4);

        PersonService.Processor<PersonServiceImpl> processor  = new PersonService.Processor<>(new PersonServiceImpl());

        //协议层 压缩协议
        arg.protocolFactory(new TCompactProtocol.Factory());
        //传输层
        arg.transportFactory(new TFramedTransport.Factory());
        arg.processorFactory(new TProcessorFactory(processor));
        THsHaServer server = new THsHaServer(arg);
        System.out.println("thrift server started");

        server.serve();

    }
}
